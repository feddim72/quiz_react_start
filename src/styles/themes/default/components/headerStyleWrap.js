import styled from 'styled-components';

export const HeaderWrap = styled.div`

    .header__wrapper {
        .header__section {
            .but-nav__v {
                display: block
            }

            .header__content {
                padding-left: 65px;
            }
        }
    }

    @media only screen and (max-width: 425px) {
        .header__wrapper {
            .header__section {
                .but-nav {
                    padding-top: 26px;
                    display: block;
                    
                    .but-nav__v{
                        display: none;
                    }
                }

                .header__content {
                    padding-left: 20px;

                    .header__name-section {
                        display: none;
                    }
                }
            }
        }
    }
    

`;


