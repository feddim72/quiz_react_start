import HomePage from './page/home/homePage';
import './styles/themes/default/theme.scss'
import store from "./components/store";
import { Route } from 'react-router';
import RouteContainerStart from './page/startPage/routeContainerStart';
import RouteContainerSelect from './page/selectPage/routeContainerSelect';
import React, { useContext } from "react";
import { SectionContext } from './components/sectionContext';


const App = () => {
  const section = useContext(SectionContext)
  return (
    <div>
      <Route exact path='/' render={() => <HomePage
        forAll={store.stateStart.forAll}
        section={store.stateStart}
        header={store.homeHeader}
        navStore={store.navStore}
      />} />

      <SectionContext.Provider value={{ section }}>
        <RouteContainerStart forAll={store.stateStart.forAll} />
        <RouteContainerSelect forAll={store.stateStart.forAll} />
      </SectionContext.Provider>
    </div>
    
  );
}

export default App;