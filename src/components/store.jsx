//header
import homeHeaderBack from '../assets/theProgramming/CanvasHome@2x.png'
import homeHeaderBackMob from '../assets/mobile/homeMobile.png'
import homeHeaderBackLabelMob from '../assets/mobile/Frame_6.png'

//for all
import icoQuizW from '../assets/buttons/forAll/QUIZ_LOGO.webp'
import icoQuiz from '../assets/buttons/forAll/QUIZ_LOGO.png'
import icoQW from '../assets/buttons/forAll/Q.webp'
import icoQ from '../assets/buttons/forAll/Q.png'

//the Technology
import icoTechnologyW from '../assets/theTechnology/ico/ico@2x.webp'
import icoTechnology from '../assets/theTechnology/ico/ico@2x.png'

//the Culture
import icoCultureW from '../assets/theCulture/ico/ico@2x.webp'
import icoCulture from '../assets/theCulture/ico/ico@2x.png'

//the Moto
import icoMotoW from '../assets/theMoto/ico/ico@2x.webp'
import icoMoto from '../assets/theMoto/ico/ico@2x.png'

//the Programming
import icoProgramW from '../assets/theProgramming/ico/ico@2x.webp'
import icoProgram from '../assets/theProgramming/ico/ico@2x.png'
import backProgramName from '../assets/theProgramming/ico/podcien.png'


//the History
import icoHistoryW from '../assets/history/ico/ico@2x.webp'
import icoHistory from '../assets/history/ico/ico@2x.png'






let store = {
    homeHeader: {
        textSection: '10 PYTAŃ / 5 KATEGORII ',
        theme: {
            backgroundPage: {
                backgroundImage: 'url(' + homeHeaderBack + ') 0 0/cover no-repeat',
                backgroundImageMobile: 'url(' + homeHeaderBackMob + ') 0 0/cover no-repeat',
                backLabelMobile: 'url(' + homeHeaderBackLabelMob + ') 60% -30% / 118% no-repeat'
            },
            backgroundNameSection: 'url(' + backProgramName + ')'
        }
    },

    navStore: [
        { id: 1, img: icoTechnology, imgW: icoTechnologyW, text: 'technologia', to: 'theTechnology' },
        { id: 2, img: icoCulture, imgW: icoCultureW, text: 'kultura', to: 'theCulture' },
        { id: 3, img: icoMoto, imgW: icoMotoW, text: 'motoryzacja', to: 'theMoto' },
        { id: 4, img: icoProgram, imgW: icoProgramW, text: 'PROGRAMOWANIE', to: 'theProgram' },
        { id: 5, img: icoHistory, imgW: icoHistoryW, text: 'historia', to: 'theHistory' }
    ],

    question:{
        select:{
            levelOne: {id:1, one:'one', two:'two', three:'three', four:'four', five:'five'},
            levelTwo:[],
            levelThree:[],
            levelFour:[],
            levelFive:[]
        }
        
    },

    stateStart: {
        forAll: {
            text: {
                textSection: 'WYBRANA KATEGORIA:',
                textButton: 'ROZPOCZHIJ',
                textSectionUse: 'WYBIERZ KATEGORIĘ',
                textSelect: 'SELECT THE CORRECT ANSWER'
            },

            icons: {
                icoQuizW: icoQuizW,
                icoQuiz: icoQuiz,
                icoQW: icoQW,
                icoQ: icoQ
            }

        }
    }

}

export default store;