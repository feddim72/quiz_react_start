import React from "react";

const HeaderBottom = (props) => {

    return (
        <div className='header__bottom'>
            <div className='header__bottom-ico'>
                <picture >
                    <source srcSet={props.theme.icons.icoSectionLabelW} alt='icoSectionLabel' />
                    <img src={props.theme.icons.icoSectionLabel} alt='icoSectionLabel' />
                </picture>

                <div className='header__bottom-line' style={props.headerBottomLine}>

                </div>

                <p>
                    {props.nameSection}
                </p>
            </div>

            <div className='header__bottom-btn'>
                <button id='button' style={props.headerBottom} onClick={props.goToPath}>
                    <p> {props.textButton} </p>
                </button>
            </div>
        </div>

    )
}

export default HeaderBottom;