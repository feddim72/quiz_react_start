import React from "react";

const Header = (props) => {
    return (
        <div>
            <div className='header__ico'>
                <picture>
                    <source srcSet={props.forAll.icons.icoQW} alt='label_Q' />
                    <img src={props.forAll.icons.icoQ} alt='label_Q' />
                </picture>
            </div>

            <div className='header__section' style={props.headerSection}>
                <div className='header__content'>
                    <div className='header__label' >
                        <picture>
                            <source srcSet={props.forAll.icons.icoQuizW} alt='label_Quiz' />
                            <img src={props.forAll.icons.icoQuiz} alt='label_Quiz' />
                        </picture>
                    </div>

                    <div className='header__name' >
                        <p className='header__name-section' style={{ backgroundImage: props.theme.backgroundNameSection }}>
                            {props.textSection}
                        </p>

                        <div className='header__name-counter' style={props.headerNameCounter} >
                            <span className='header__name-number'>
                                <span> 1 </span> / <span> 10</span>
                            </span>
                        </div>
                    </div>

                    <div className='header__question'>
                        <p>
                            {props.textQuestion}
                        </p>
                    </div>
                </div>

                <div className='but-nav'>
                    <button className='but-nav__v' onClick={props.goToPreviousPath} />
                    <button className='but-nav__x' onClick={props.goToHomePath} />
                </div>
            </div>
        </div>
    )
}

export default Header;