export const goToPreviousPathUtils = (e, history) => {
    e.preventDefault()
    history.goBack()
}

export const goToHomePathUtils = (e, history) => {
    e.preventDefault()
    history.push('/')
}

export const goToPathUtils = (e, history, location, section) => {
    e.preventDefault()
    const path = location.pathname
    const selectPath = path + section
    history.push(selectPath)

}

export const useAnswerUtils = (id, answer, setAnswer) => {

    setAnswer(
        answer.map(a => {
            a.completed = false
            if (a.id === id) {
                a.completed = !a.completed
            }
            return a
        })
    )
}