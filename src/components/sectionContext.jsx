import { createContext } from "react";

//header
import homeHeaderBack from '../assets/theProgramming/CanvasHome@2x.png'
import homeHeaderBackMob from '../assets/mobile/homeMobile.png'
import homeHeaderBackLabelMob from '../assets/mobile/Frame_6.png'

//the Technology
import icoTechnologyW from '../assets/theTechnology/ico/ico@2x.webp'
import icoTechnology from '../assets/theTechnology/ico/ico@2x.png'
import TechnologyStart from '../assets/theTechnology/CanvasStart.png'
import TechnologySelect from '../assets/theTechnology/CanvasStart.png'
import TechnologyDrag from '../assets/theTechnology/CanvasStart.png'
import TechnologyEnd from '../assets/theTechnology/CanvasEnd.png'
import TechnologyStartMobile from '../assets/mobile/technologyStart.png'
import TechnologyBackLabelMob from '../assets/mobile/Frame_6.png'
import backTechnologyName from '../assets/theTechnology/ico/podcien.png'
import buttonArrowTechnology from '../assets/buttons/technology/arrowPlay.png'
import buttonPlayTechnology from '../assets/buttons/technology/play.png'
import buttonPlayTechnologyHover from '../assets/buttons/technology/playHover.png'



//the Culture
import icoCultureW from '../assets/theCulture/ico/ico@2x.webp'
import icoCulture from '../assets/theCulture/ico/ico@2x.png'
import CultureStart from '../assets/theCulture/CanvasStart.png'
import CultureSelect from '../assets/theCulture/CanvasStart.png'
import CultureDrag from '../assets/theCulture/CanvasStart.png'
import CultureEnd from '../assets/theCulture/CanvasEnd.png'
import backCultureName from '../assets/theCulture/ico/podcien.png'
import buttonArrowCulture from '../assets/buttons/culture/arrowPlay.png'
import buttonPlayCulture from '../assets/buttons/culture/play.png'
import buttonPlayCultureHover from '../assets/buttons/culture/playHover.png'





//the Moto
import icoMotoW from '../assets/theMoto/ico/ico@2x.webp'
import icoMoto from '../assets/theMoto/ico/ico@2x.png'
import motoStartMobile from '../assets/mobile/moto.png'
import motoStart from '../assets/theMoto/CanvasStart.png'
import motoSelect from '../assets/theMoto/CanvasStart.png'
import motoDrag from '../assets/theMoto/CanvasStart.png'
import motoEnd from '../assets/theMoto/CanvasEnd.png'
import backMotoName from '../assets/theMoto/ico/podcien.png'
import buttonArrowMoto from '../assets/buttons/moto/arrowPlay.png'
import buttonPlayMoto from '../assets/buttons/moto/play.png'
import buttonPlayMotoHover from '../assets/buttons/moto/playHover.png'




//the Programming
import icoProgramW from '../assets/theProgramming/ico/ico@2x.webp'
import icoProgram from '../assets/theProgramming/ico/ico@2x.png'
import programStart from '../assets/theProgramming/CanvasStart.png'
import programSelect from '../assets/theProgramming/CanvasSelect.png'
import programDrag from '../assets/theProgramming/CanvasDrag.png'
import programEnd from '../assets/theProgramming/CanvasEnd.png'
import backProgramName from '../assets/theProgramming/ico/podcien.png'
import buttonArrowProgram from '../assets/buttons/program/arrowPlay.png'
import buttonPlayProgram from '../assets/buttons/program/play.png'
import buttonPlayProgramHover from '../assets/buttons/program/playHover.png'


import counter from '../assets/theProgramming/ico/counter.png'
import counterW from '../assets/theProgramming/ico/counter@2x.png'
import buttonActiveProgram from '../assets/theProgramming/ico/active.png'



//the History
import icoHistoryW from '../assets/history/ico/ico@2x.webp'
import icoHistory from '../assets/history/ico/ico@2x.png'
import historyStartMobile from '../assets/mobile/history.png'
import historyBackMobBlue from '../assets/mobile/backMobBlue.png'
import historyStart from '../assets/history/CanvasStart.png'
import HistorySelect from '../assets/history/CanvasSelect.png'
import HistoryDrag from '../assets/history/CanvasDrag.png'
import HistoryEnd from '../assets/history/CanvasEnd.png'
import backHistoryName from '../assets/history/ico/podcien.png'
import buttonArrowHistory from '../assets/buttons/history/arrowPlay.png'
import buttonPlayHistory from '../assets/buttons/history/play.png'
import buttonArrowHistoryMobile from '../assets/buttons/history/arrowButton.png'
import buttonPlayHistoryHover from '../assets/buttons/history/playHover.png'

export const SectionContext = createContext({
    section: [
        {
            id: 1, link: 'theTechnology', nameSection: 'Technology',

            question: {
                selectOne: [
                    { id: 1, text: 'one' },
                    { id: 2, text: 'two' },
                    { id: 3, text: 'three' },
                    { id: 4, text: 'four' },
                    { id: 5, text: 'five' },
                    { id: 6, text: 'five' },
                    { id: 7, textQuestion: 'five' },
                ]
            },

            theme: {
                backgroundPage: {
                    backgroundImage: 'url(' + TechnologyStart + ') 0 0/cover no-repeat',
                    backgroundImageMobile: 'url(' + TechnologyStartMobile + ') 0 0 / 105% no-repeat',
                    headerBackLabelMob: 'url(' + TechnologyBackLabelMob + ') 0 0 / 100% no-repeat'
                },

                icons: {
                    icoSectionLabelW: icoTechnologyW,
                    icoSectionLabel: icoTechnology,
                    buttonArrow: 'url(' + buttonArrowTechnology + ') 256px 8px no-repeat, url(' + buttonPlayTechnology + ') -2px -2px / 104%',
                    buttonHover: 'url(' + buttonArrowTechnology + ') 256px 8px no-repeat, url(' + buttonPlayTechnologyHover + ') -2px -2px / 104%',
                    buttonHoverMobile: 'url(' + buttonArrowTechnology + ') 198px 8px / 24% no-repeat, url(' + buttonPlayTechnologyHover + ') -2px -2px / 104%',
                    buttonArrowMobile: 'url(' + buttonArrowTechnology + ') 198px 8px / 24% no-repeat, url(' + buttonPlayTechnology + ') -2px -2px / 104%',
                    shadowButtonMobile: '2.76px 2.76px 2.76px rgb(91 91 239 / 40%)',
                    shadowButton: '2.76px 2.76px 2.76px rgb(91 91 239 / 40%)',
                    borderButtonMobile: '1px solid #8a57f1',
                    headerBottomLine: '#F94AB3'

                },

                backgroundNameSection: 'url(' + backTechnologyName + ')'
            }
        },

        {
            id: 2, link: 'theCulture', nameSection: 'Culture',

            question: {
                selectOne: [
                    { id: 1, text: 'one' },
                    { id: 2, text: 'two' },
                    { id: 3, text: 'three' },
                    { id: 4, text: 'four' },
                    { id: 5, text: 'five' },
                    { id: 6, text: 'five' },
                ]
            },

            theme: {
                backgroundPage: {
                    backgroundImage: 'url(' + CultureStart + ') 0 0/cover no-repeat',
                    backgroundImageMobile: 'url(' + historyStartMobile + ') -268px -192px / 366% no-repeat',
                    headerBackLabelMob: 'url(' + historyBackMobBlue + ') 0 0 / 100% no-repeat'
                },

                icons: {
                    icoSectionLabelW: icoCultureW,
                    icoSectionLabel: icoCulture,
                    buttonArrow: 'url(' + buttonArrowCulture + ') 256px 11px no-repeat, url(' + buttonPlayCulture + ') -2px -2px / 104%',
                    buttonHover: 'url(' + buttonArrowCulture + ') 256px 11px no-repeat, url(' + buttonPlayCultureHover + ') -2px -2px / 104%',
                    buttonHoverMobile: 'url(' + buttonArrowMoto + ') 198px 4px / 28% no-repeat, url(' + buttonPlayMoto + ') -2px -2px / 104%',
                    buttonArrowMobile: 'url(' + buttonArrowMoto + ') 198px 4px / 28% no-repeat, url(' + buttonPlayMotoHover + ') -2px -2px / 104%',
                    headerBottomLine: '#F94AB3'
                },

                backgroundNameSection: 'url(' + backCultureName + ') '
            }
        },

        {
            id: 3, link: 'theMoto', nameSection: 'Moto',

            question: {
                selectOne: [
                    { id: 1, text: 'one' },
                    { id: 2, text: 'two' },
                    { id: 3, text: 'three' },
                    { id: 4, text: 'four' },
                    { id: 5, text: 'five' },
                    { id: 6, text: 'five' },
                ]
            },

            theme: {
                backgroundPage: {
                    backgroundImage: 'url(' + motoStart + ') 0 0/cover no-repeat',
                    backgroundImageMobile: 'url(' + motoStartMobile + ') -538px -24px / 256% no-repeat',
                    headerBackLabelMob: 'url(' + historyBackMobBlue + ') 0 0 / 100% no-repeat'
                },

                icons: {
                    icoSectionLabelW: icoMotoW,
                    icoSectionLabel: icoMoto,
                    buttonArrow: 'url(' + buttonArrowMoto + ') 256px 8px no-repeat, url(' + buttonPlayMoto + ') -2px -2px / 104%',
                    buttonHover: 'url(' + buttonArrowMoto + ') 256px 8px no-repeat, url(' + buttonPlayMotoHover + ') -2px -2px / 104%',
                    buttonHoverMobile: 'url(' + buttonArrowMoto + ') 198px 4px / 28% no-repeat, url(' + buttonPlayMotoHover + ') -2px -2px / 104%',
                    buttonArrowMobile: 'url(' + buttonArrowMoto + ') 198px 4px / 28% no-repeat, url(' + buttonPlayMoto + ') -2px -2px / 104%',
                    headerBottomLine: '#0000FF'

                },

                backgroundNameSection: 'url(' + backMotoName + ')'
            }
        },

        {
            id: 4, link: 'theProgram', nameSection: 'Program',

            question: {
                selectQuestionOne: '1. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum?',
                selectOne: [
                    { id: 1, completed: false, text: 'one' },
                    { id: 2, completed: false, text: 'two' },
                    { id: 3, completed: false, text: 'three' },
                    { id: 4, completed: false, text: 'four' },
                    { id: 5, completed: false, text: 'five' },
                ]
            },


            theme: {
                backgroundPage: {
                    backgroundImageSelect: 'url(' + programSelect + ') 0 0/cover no-repeat',
                    backgroundCounter: 'url(' + counter + ') -22px -24px/ 144% no-repeat',
                    backgroundImage: 'url(' + programStart + ') 0 0/cover no-repeat',
                    backgroundImageMobile: 'url(' + programStart + ') 0px -144px / 400% no-repeat',
                    headerBackLabelMob: 'url(' + homeHeaderBackLabelMob + ') 0 0 / 100% no-repeat'
                },

                icons: {
                    icoSectionLabelW: icoProgramW,
                    icoSectionLabel: icoProgram,
                    buttonArrow: 'url(' + buttonArrowProgram + ') 256px 8px no-repeat, url(' + buttonPlayProgram + ') -2px -2px / 104%',
                    buttonQuestion: 'url(' + buttonPlayProgram + ') -2px -2px / 104%',
                    buttonQuestionActive: 'url(' + buttonActiveProgram + ') 256px 2px no-repeat, url(' + buttonPlayProgram + ') -2px -2px / 104%',
                    buttonQuestionHoverActive: 'url(' + buttonActiveProgram + ') 256px 2px no-repeat,url(' + buttonPlayProgramHover + ') -2px -2px / 104%',
                    buttonQuestionHover: 'url(' + buttonPlayProgramHover + ') -2px -2px / 104%',
                    buttonHover: 'url(' + buttonArrowProgram + ') 256px 8px no-repeat, url(' + buttonPlayProgramHover + ') -2px -2px / 104%',
                    buttonHoverMobile: 'url(' + buttonArrowProgram + ') 198px 4px / 24% no-repeat, url(' + buttonPlayProgramHover + ') -2px -2px / 104%',
                    buttonArrowMobile: 'url(' + buttonArrowProgram + ') 198px 4px / 24% no-repeat, url(' + buttonPlayProgram + ') -2px -2px / 104%',

                },

                backgroundNameSection: 'url(' + backProgramName + ')'
            }
        },

        {
            id: 5, link: 'theHistory', nameSection: 'HISTORIA',

            question: {
                selectOne: [
                    { id: 1, text: 'one' },
                    { id: 2, text: 'two' },
                    { id: 3, text: 'three' },
                    { id: 4, text: 'four' },
                    { id: 5, text: 'five' },
                ]
            },

            theme: {
                backgroundPage: {
                    backgroundImage: 'url(' + historyStart + ') 0 0/cover no-repeat',
                    backgroundImageMobile: 'url(' + historyStartMobile + ') -554px -87px / 366% no-repeat',
                    headerBackLabelMob: 'url(' + historyBackMobBlue + ') 0 0 / 100% no-repeat'
                },

                icons: {
                    icoSectionLabelW: icoHistoryW,
                    icoSectionLabel: icoHistory,
                    buttonArrow: 'url(' + buttonArrowHistory + ') 256px 8px no-repeat, url(' + buttonPlayHistory + ') -2px -2px / 104% ',
                    buttonHover: 'url(' + buttonArrowHistory + ') 256px 8px no-repeat, url(' + buttonPlayHistoryHover + ') -2px -2px / 104% ',
                    buttonHoverMobile: 'url(' + buttonArrowHistoryMobile + ') 198px 4px / 24% no-repeat, url(' + buttonPlayMotoHover + ') -2px -2px / 104% ',
                    buttonArrowMobile: 'url(' + buttonArrowHistoryMobile + ') 198px 4px / 24% no-repeat, linear-gradient(45deg, rgba(51,12,127,1) 0%, rgba(79,38,158,1) 55%, rgba(111,63,203,1) 77%, rgba(107,56,207,1) 94%, rgba(126,84,209,1) 100%)',

                },

                backgroundNameSection: 'url(' + backHistoryName + ')'
            }
        }
    ]
})

