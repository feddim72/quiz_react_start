import React from "react";
import NavItem from "./navItem";



const Nav = (props) => {

    let navItems = props.navStore.map(p => <NavItem
        img={p.img}
        imgW={p.imgW}
        text={p.text}
        key={p.id}
        to={p.to}
    />)

    return (
        <nav>
            <p className='nav__text-use'>
                {props.forAll.textSectionUse}
            </p>

            {navItems}
        </nav>
    )
}

export default Nav;