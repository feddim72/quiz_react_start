import React from "react";
import { NavLink } from "react-router-dom";



const NavItem = (props) => {

    return (
        <div>

            <NavLink className='nav__item' to={`/${props.to}`}>

                <div className='nav__ico'>
                    <picture>
                        <source srcSet={props.imgW} id={props.id} alt='nav__ico' />
                        <img src={props.img} id={props.id} alt='nav__ico' />
                    </picture>
                </div>

                <div className='nav__line'>

                </div>

                <div className='nav__text'>
                    <p>
                        {props.text}
                    </p>
                </div>
                
            </NavLink>

        </div>
    )
}

export default NavItem;