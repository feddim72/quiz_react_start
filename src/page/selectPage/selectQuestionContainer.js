import React from "react";
import SelectItem from "./selectItem";

const SelectQuestionContainer = (props) => {
    let answer = props.question.selectOne.map(p => <SelectItem
        useAnswer={props.useAnswer}
        text={p.text}
        id={p.id}
        key={p.id}
        active={p.completed}
    />)

    return (

        <div className='answer__block'>
            <div className='answer__content'>
                {answer}
            </div>
        </div>

    )
}

export default SelectQuestionContainer;