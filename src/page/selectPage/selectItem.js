import React from "react";

const SelectItem = (props) => {
    return (
        <div>
            <div className={props.active ? 'answer__item answer__item--active' : 'answer__item'} onClick={() => props.useAnswer(props.id)} >
                <p>
                    {props.text}
                </p>
            </div>
        </div>
    )
}

export default SelectItem;