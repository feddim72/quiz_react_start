import React from "react";
import { useContext } from "react";
import { Route } from "react-router";
import { SectionContext } from "../../components/sectionContext";
import { HeaderWrap } from "../../styles/themes/default/components/headerStyleWrap";
import SelectPage from "./selectPage";




const RouteContainerSelect = (props) => {
    const { section } = useContext(SectionContext);
    let RouteItems = section.section.map(p => <Route exact path={`/${p.link}/select`} key={p.id}
        render={() => <SelectPage
            forAll={props.forAll}
            theme={p.theme}
            question={p.question}
        />}
    />)

    return (
        <HeaderWrap>
            {RouteItems}
        </HeaderWrap>
    )
}

export default RouteContainerSelect;