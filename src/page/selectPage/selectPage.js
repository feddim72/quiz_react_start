import React, { useEffect, useState } from "react";
import Header from "../../components/header/header";
import styled from 'styled-components';
import { useHistory } from "react-router";
import { goToHomePathUtils, goToPreviousPathUtils, useAnswerUtils } from "../../components/utils/utils";
import SelectQuestionContainer from "./selectQuestionContainer";




const SelectPage = (props) => {

    const [answer, setAnswer] = useState(props.question.selectOne)
    let history = useHistory()

    const goToPreviousPath = (e) => goToPreviousPathUtils(e, history)
    const useAnswer = (id) => useAnswerUtils(id, answer, setAnswer)
    const goToHomePath = (e) => goToHomePathUtils(e, history)

    const mediaMatch = window.matchMedia('(max-width: 425px)');
    const [matches, setMatches] = useState(mediaMatch.matches);

    useEffect(() => {
        const handler = e => setMatches(e.matches);
        mediaMatch.addListener(handler);
        return () => mediaMatch.removeListener(handler);
    });

    const styles = {
        headerWrapper: isMobile => ({
            background: isMobile ? props.theme.backgroundPage.backgroundImageMobile : props.theme.backgroundPage.backgroundImageSelect
        }),

        headerNameCounter: isMobile => ({
            background: isMobile ? '' : props.theme.backgroundPage.backgroundCounter
        }),

        headerSection: isMobile => ({
            background: isMobile ? props.theme.backgroundPage.headerBackLabelMob : ''
        }),


    };

    const SelectWrap = styled.div`

        .header {
            &__wrapper {
                .header {
                    &__section {
                        .header {
                            &__content {
                                .header {
                                    &__name {
                                        width: 100%;
                                        display: flex;
                                        justify-content: center;
                                        margin-left: 110px;
                                    }

                                    &__name-section {
                                        width:70%;
                                    }

                                    &__name-counter {
                                        display: block;
                                    }

                                    &__question {
                                        display: block;
                                    }
                                }
                            }
                        }
                    }
                }


                .answer__block {
                    .answer__content {
                        

                        .answer__item {
                            display: flex;
                            background: ${props.theme.icons.buttonQuestion} ;
                        
                            :hover {
                                background: ${props.theme.icons.buttonQuestionHover};
                            }
                        }

                        .answer__item--active {
                            background: ${props.theme.icons.buttonQuestionHoverActive} ;

                            :hover {
                                background: ${props.theme.icons.buttonQuestionHoverActive};
                            }
                        }
                    }
                }
            }
        }

    `

    return (
        <SelectWrap>
            <div className='header__wrapper' style={styles.headerWrapper(matches)} >

                <Header
                    forAll={props.forAll}
                    theme={props.theme}
                    textSection={props.forAll.text.textSelect}
                    headerSection={styles.headerSection(matches)}
                    goToPreviousPath={goToPreviousPath}
                    goToHomePath={goToHomePath}
                    headerNameCounter={styles.headerNameCounter(matches)}
                    textQuestion={props.question.selectQuestionOne}
                />

                <SelectQuestionContainer
                    question={props.question}
                    useAnswer={useAnswer}
                />

            </div >
        </SelectWrap>

    )
}

export default SelectPage;