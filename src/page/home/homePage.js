import React, { useEffect, useState } from "react";
import Header from "../../components/header/header";
import Nav from "../../components/nav/nav";



const HomePage = (props) => {
    const mediaMatch = window.matchMedia('(max-width: 425px)');
    const [matches, setMatches] = useState(mediaMatch.matches);

    useEffect(() => {
        const handler = e => setMatches(e.matches);
        mediaMatch.addListener(handler);
        return () => mediaMatch.removeListener(handler);
    });

    const styles = {
        container: isBackground => ({
            background: isBackground ? props.header.theme.backgroundPage.backgroundImageMobile : props.header.theme.backgroundPage.backgroundImage
        })
    };

    const stylesBackLabel = {
        container: isBackground => ({
            background: isBackground ? props.header.theme.backgroundPage.backLabelMobile : ''
        })
    };

    return (

        <div className='header__wrapper' style={styles.container(matches)} >

            <Header
                forAll={props.forAll}
                section={props.stateStart}
                theme={props.header.theme}
                textSection={props.header.textSection}
                stylesBackLabel={stylesBackLabel.container(matches)}
            />

            <Nav
                navStore={props.navStore}
                forAll={props.forAll}
            />
            
        </div>
    )
}



export default HomePage;