import React, { useContext } from "react";
import { Route } from "react-router";
import { SectionContext } from "../../components/sectionContext";
import { HeaderWrap } from "../../styles/themes/default/components/headerStyleWrap";
import StartPage from "./startPage";



const RouteContainerStart = (props) => {
    const {section} = useContext(SectionContext);
    let RouteItems = section.section.map(p => <Route exact path={`/${p.link}`} key={p.id}
        render={() => <StartPage
            forAll={props.forAll}
            theme={p.theme}
            nameSection={p.nameSection}
        />}
    />)
    
    return (
        <HeaderWrap>
            {RouteItems}
        </HeaderWrap>
    )
}

export default RouteContainerStart;