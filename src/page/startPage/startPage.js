import React, { useEffect, useState } from "react";
import Header from "../../components/header/header";
import HeaderBottom from "../../components/header/headerBottom";
import styled from 'styled-components';
import { useHistory, useLocation } from "react-router";
import { goToPreviousPathUtils, goToPathUtils, goToHomePathUtils } from "../../components/utils/utils";

const StartPage = (props) => {

    const mediaMatch = window.matchMedia('(max-width: 425px)')
    const [matches, setMatches] = useState(mediaMatch.matches)

    useEffect(() => {
        const handler = e => setMatches(e.matches)
        mediaMatch.addListener(handler)
        return () => mediaMatch.removeListener(handler)
    })

    let history = useHistory()
    let location = useLocation()

    const goToPreviousPath = (e) => goToPreviousPathUtils(e, history)

    const goToHomePath = (e) => goToHomePathUtils(e, history)

    const goToPath = (e) => goToPathUtils(e, history, location, `${'/select'}`)

    const styles = {
        headerWrapper: isMobile => ({
            background: isMobile ? props.theme.backgroundPage.backgroundImageMobile : props.theme.backgroundPage.backgroundImage
        }),

        headerSection: isMobile => ({
            background: isMobile ? props.theme.backgroundPage.headerBackLabelMob : ''
        }),

        headerBottomLine: isMobile => ({
            background: isMobile ? props.theme.icons.headerBottomLine : ''
        }),

        headerBottom: isMobile => ({
            background: isMobile ? `${props.theme.icons.buttonArrowMobile}` : `${props.theme.icons.buttonArrow}`,
            boxShadow: isMobile ? `${props.theme.icons.shadowButtonMobile}` : `${props.theme.icons.shadowButton}`,
            border: isMobile ? `${props.theme.icons.borderButtonMobile}` : ''

        }),
    }

    const StartWrap = styled.div`

        .header {
            &__wrapper {
                .header {
                    &__section {
                        .header {
                            &__content {
                                .header {
                                    &__label {
                                        margin-bottom: 70px
                                    }
                                }
                            }
                        }
                    }
        
                    &__bottom {
                        .header {
                            &__bottom-btn {
                                button {
                                    ${styles.headerBottom(matches)}
            
                                    &:hover{
                                        background: ${props.theme.icons.buttonHover};
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        @media only screen and (max-width: 425px) {
            .header {
                &__wrapper {
                    .header {
                        &__bottom {
                            .header {
                                &__bottom-btn {
                                    button {
                                        &:hover{
                                            background: ${props.theme.icons.buttonHoverMobile};
                                        }
                        
                                    }
                                }
                            }
                        }
                    }
                }    
            }
        }

    `


    return (
        <StartWrap>
            <div className='header__wrapper' style={styles.headerWrapper(matches)} >

                <Header
                    forAll={props.forAll}
                    theme={props.theme}
                    textSection={props.forAll.text.textSection}
                    headerSection={styles.headerSection(matches)}
                    goToPreviousPath={goToPreviousPath}
                    goToHomePath={goToHomePath}
                />

                <HeaderBottom
                    nameSection={props.nameSection}
                    textButton={props.forAll.text.textButton}
                    theme={props.theme}
                    headerBottomLine={styles.headerBottomLine(matches)}
                    goToPath={goToPath}
                />

            </div >
        </StartWrap>
    )
}

export default StartPage;